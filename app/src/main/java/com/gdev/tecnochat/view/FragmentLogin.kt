package com.gdev.tecnochat.view

import android.os.Bundle
import android.os.StrictMode
import android.text.TextUtils.isEmpty
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.gdev.tecnochat.R
import com.gdev.tecnochat.databinding.FragmentLoginBinding

class FragmentLogin: Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        binding.login.setOnClickListener{
            var text = binding.ip.text.toString()
            val ip = if(isEmpty(text)) "192.168.100.191" else text
            text = binding.port.text.toString()
            val port = if(isEmpty(text)) 7800 else Integer.parseInt(text)
            val user = binding.userName.text.toString()
            val pass = binding.userPass.text.toString()
            login(ip, port, user, pass)
        }
    }
    private fun login(ip: String, port: Int, user: String, pass: String) {
        val data = Bundle()
        data.putInt("port", port)
        data.putString("ip", ip)
        data.putString("pass", pass)
        data.putString("user", user)
        findNavController().navigate(R.id.action_fragmentLogin_to_fragmentChat, data)
    }
}