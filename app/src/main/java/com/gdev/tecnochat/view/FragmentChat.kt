package com.gdev.tecnochat.view

import android.app.Dialog
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import com.gdev.tecnochat.model.Message
import com.gdev.tecnochat.adapter.UserOnlineAdapter
import com.gdev.tecnochat.utils.Utils
import com.gdev.tecnochat.adapter.ChatAdapter
import com.gdev.tecnochat.databinding.FragmentChatBinding
import com.gdev.tecnochat.model.DataBaseApp
import com.gdev.tecnochat.model.MessageDao
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.PrintWriter
import java.net.Socket

class FragmentChat: Fragment() {
    private var _binding: FragmentChatBinding? = null
    private val binding get() = _binding!!

    private var input: BufferedReader? = null
    private var output: PrintWriter? = null
    private var cnx: Socket? = null
    private var currentUser = ""
    private lateinit var hilo: Chat

    private lateinit var messages: MutableList<Message>
    private lateinit var adapterMessages: ChatAdapter
    private lateinit var adapterUsersOnline: UserOnlineAdapter
    private lateinit var messageDao: MessageDao

    private lateinit var progressDialog: Dialog

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentChatBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressDialog =
            CustomProgressDialog.progressDialog(binding.root.context)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        arguments?.let {
            val ip = it.getString("ip")
            val port = it.getInt("port")
            currentUser = it.getString("user", "")
            val pass = it.getString("pass")
            progressDialog.show()
            authenticate(ip!!, port, currentUser, pass!!)
        }
        messages = mutableListOf()
        messageDao = DataBaseApp.getInstance(requireContext()).messages()
        adapterMessages = ChatAdapter(requireContext())
        binding.listChat.adapter = adapterMessages

        adapterUsersOnline = UserOnlineAdapter()
        binding.usersOnline.adapter = adapterUsersOnline

        binding.send.setOnClickListener {
            hilo.sendMessage(binding.message.text.toString())
            binding.message.setText("")
        }
    }

    private fun authenticate(ip: String, port: Int, userName: String, userPass: String){
        hilo = Chat(ip, port, userName, userPass)
        hilo.start()
    }

    override fun onDestroy() {
        super.onDestroy()
        hilo.finish()
    }
    inner class Chat(
        private val ip: String,
        private val port: Int,
        private val user: String,
        private val pass: String
    ): Thread(){
        private var ejecutar = true
        override fun run() {
            super.run()
            try {
                cnx = Socket(ip, port)
                input = BufferedReader(InputStreamReader(cnx?.getInputStream()))
                output = PrintWriter(cnx?.getOutputStream(), true)
                var response: String?
                login(user, pass)
                while (ejecutar){
                    response = input?.readLine()
                    if(response != null)
                        receiveMessage(response)
                }
            }catch (e: IOException){
                Log.v("IOException:", e.message!!)
                alert("IOException", e.message!!)
            }
        }
        fun finish(){
            sendMessage("/salir")
            ejecutar = false
            cnx?.close()
        }
        fun sendMessage(text: String){
            if(output != null){
                val message = JSONObject()
                message.put("type", Utils.MESSAGE)
                message.put("message", text)
                output?.println(message.toString())
                addMessage(user, text, 2)
                if(text.equals("/salir"))
                    this@FragmentChat.findNavController().popBackStack()
            }
        }
        fun login(userName: String, userPass: String){
            var request = JSONObject()
            request.put("type", Utils.MESSAGE_SERVER)
            request.put("username", userName)
            request.put("password", userPass)
            output?.println(request.toString())
        }
        private fun receiveMessage(response: String){
            val message = JSONObject(response)
            when(message.getString("type")){
                Utils.MESSAGE -> {
                    activity?.runOnUiThread{
                        addMessage(message.getString("sender"), message.getString("message"), 1)
                    }
                }
                Utils.MESSAGE_SERVER -> {
                    activity?.runOnUiThread{
                        addMessage(message.getString("sender"), message.getString("message"), 0)
                    }
                }
                Utils.MESSAGE_LOGIN -> {
                    val online = message.getBoolean("online")
                    activity?.runOnUiThread{ progressDialog.dismiss() }
                    if(!online){
                        activity?.runOnUiThread{
                            alert("Error al iniciar sesión", "Usuario o contraseña incorrecta.")
                        }
                    }else{
                        activity?.runOnUiThread{
                            messageDao.getAll(currentUser).observe(
                                viewLifecycleOwner,
                                Observer{ list ->
                                    adapterMessages.setList(list)
                                }
                            )
                        }

                    }
                }
                  Utils.USERS_ONLINE -> {
                    val a = message.getJSONArray("users_online")
                    val new = mutableListOf<String>()
                    for(i in 0 until a.length())
                        new.add(a.getString(i))
                    activity?.runOnUiThread{adapterUsersOnline.setList(new)}
                }
            }
        }
        private fun addMessage(user: String, message: String, type: Int){
            messageDao.insert(Message(currentUser, type, message, user))
            /*messages.add(Message(currentUser, type, message, user))
            adapterMessages.setList(messages)*/
        }
        private fun alert(title: String, message: String){
            activity?.runOnUiThread {
                progressDialog.dismiss()
                AlertDialog.Builder(this@FragmentChat.requireContext())
                    .setTitle(title)
                    .setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Entendido"){dialog, which ->
                        this@FragmentChat.findNavController().popBackStack()
                    }
                    .show()
            }
        }
    }
}