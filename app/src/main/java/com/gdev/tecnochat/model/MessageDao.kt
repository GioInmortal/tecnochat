package com.gdev.tecnochat.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MessageDao{
    @Query("SELECT * FROM messages WHERE userName=:currentUser")
    fun getAll(currentUser: String): LiveData<List<Message>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(message: Message)
}