package com.gdev.tecnochat.model

import androidx.room.Entity
import java.io.Serializable
import androidx.room.PrimaryKey
@Entity(tableName = "messages")
data class Message(
    val userName: String,
    val type: Int,
    val message: String,
    val sender: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
): Serializable
