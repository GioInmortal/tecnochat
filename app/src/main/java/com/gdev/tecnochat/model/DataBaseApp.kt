package com.gdev.tecnochat.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
@Database(entities = [Message::class], version = 1)
abstract class DataBaseApp: RoomDatabase() {
    abstract fun messages(): MessageDao
    companion object{
        private var INSTANCE: DataBaseApp? = null
        fun getInstance(context: Context): DataBaseApp{
            if(INSTANCE == null){
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    DataBaseApp::class.java, "tecnochat-database"
                ).allowMainThreadQueries().fallbackToDestructiveMigration().build()
            }
            return INSTANCE!!
        }
    }
}