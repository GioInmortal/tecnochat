package com.gdev.tecnochat.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gdev.tecnochat.R
import com.gdev.tecnochat.databinding.ItemUserOnlineBinding

class UserOnlineAdapter: RecyclerView.Adapter<UserOnlineAdapter.ViewHolder>() {
    private var items = listOf<String>()
    fun setList(list: List<String>){
        this.items = list
        notifyDataSetChanged()
    }
    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val binding = ItemUserOnlineBinding.bind(view)
        fun bind(userName: String){
            binding.userName.text = userName
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_user_online, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = items.size
}