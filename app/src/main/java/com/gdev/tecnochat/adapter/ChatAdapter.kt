package com.gdev.tecnochat.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.gdev.tecnochat.model.Message
import com.gdev.tecnochat.R
import com.gdev.tecnochat.databinding.ItemMessageReceivedBinding
import com.gdev.tecnochat.databinding.ItemMessageSentBinding
import com.gdev.tecnochat.databinding.ItemMessageSystemBinding
import java.text.SimpleDateFormat
import java.util.*

class ChatAdapter(private val context: Context): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private var items = listOf<Message>()
    fun setList(list: List<Message>){
        this.items = list
        notifyDataSetChanged()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder{
        return when(viewType){
            messageSystem -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_message_system, parent, false)
                MessageSystem(view)
            }
            messageSent -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_message_sent, parent, false)
                MessageSent(view)
            }
            messageReceived -> {
                val view = LayoutInflater.from(context)
                    .inflate(R.layout.item_message_received, parent, false)
                MessageReceived(view)
            }
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when(item.type){
            messageSystem -> (holder as MessageSystem).bind(item)
            messageSent -> (holder as MessageSent).bind(item)
            messageReceived -> (holder as MessageReceived).bind(item)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }
    override fun getItemCount(): Int {
        return items.size
    }

    inner class MessageSent(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = ItemMessageSentBinding.bind(itemView)
        fun bind(item: Message) {
            binding.message.text = item.message
            binding.date.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(Date())
        }
    }
    inner class MessageReceived(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding = ItemMessageReceivedBinding.bind(itemView)
        fun bind(item: Message) {
            with(binding){
                userName.text = item.sender
                message.text = item.message
                date.text = SimpleDateFormat("dd/MM/yyyy HH:mm").format(Date())
            }
        }
    }
    inner class MessageSystem(itemView: View): RecyclerView.ViewHolder(itemView) {
        val binding = ItemMessageSystemBinding.bind(itemView)
        fun bind(item: Message) {
            binding.message.text = item.message
        }
    }
    companion object{
        private const val messageSystem = 0
        private const val messageReceived = 1
        private const val messageSent = 2
    }
}