package com.gdev.tecnochat.utils

object Utils {

    const val MESSAGE_SERVER = "message_server"
    const val MESSAGE_LOGIN = "message_login"
    const val MESSAGE = "message"
    const val USERS_ONLINE = "users_online"
}